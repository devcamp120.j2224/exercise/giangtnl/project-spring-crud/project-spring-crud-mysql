package com.devcamp.rest_api.Service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.rest_api.Model.Ward;
import com.devcamp.rest_api.Repository.WardRepository;

@Service
public class WardService {
    @Autowired
    WardRepository vWardRepository;

    public ArrayList<Ward> getWardList() {
        ArrayList<Ward> listWard = new ArrayList<>();
        vWardRepository.findAll().forEach(listWard::add);
        return listWard;
    }

}
