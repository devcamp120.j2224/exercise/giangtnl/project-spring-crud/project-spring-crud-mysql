package com.devcamp.rest_api.Controller;

import java.util.ArrayList;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.rest_api.Model.District;
import com.devcamp.rest_api.Model.Province;
import com.devcamp.rest_api.Model.Ward;
import com.devcamp.rest_api.Repository.DistrictRepository;
import com.devcamp.rest_api.Repository.ProvinceRepository;
import com.devcamp.rest_api.Repository.WardRepository;
import com.devcamp.rest_api.Service.DistrictServirce;
import com.devcamp.rest_api.Service.ProvinceService;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

@RestController
@RequestMapping("/")
@CrossOrigin(value = "*", maxAge = -1)
public class ApiController {
    @Autowired
    DistrictRepository districtRepository;

    @Autowired
    ProvinceRepository provinceRepository;

    @Autowired
    ProvinceService provinceService;

    @Autowired
    WardRepository wardRepository;

    @Autowired
    DistrictServirce districtServirce;

    // gọi all province
    @GetMapping("/province")
    public ResponseEntity<List<Province>> getAllProvince() {
        try {

            return new ResponseEntity<>(provinceService.getProvinceList(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    // gọi province theo id

    @GetMapping("/province/{id}")
    public ResponseEntity<Province> getAllProvinceById(@PathVariable("id") int id) {
        Optional<Province> province = provinceRepository.findById(id);
        if (province.isPresent()) {
            return new ResponseEntity<>(province.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    // gọi all district
    @GetMapping("/district")
    public ResponseEntity<List<District>> getAllDistrict() {
        try {

            return new ResponseEntity<>(districtServirce.getDistrictList(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    // gọi district theo id

    @GetMapping("/district/{id}")
    public ResponseEntity<District> getAllDistrictById(@PathVariable("id") int id) {
        Optional<District> district = districtRepository.findById(id);
        if (district.isPresent()) {
            return new ResponseEntity<>(district.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    // gọi all ward
    @GetMapping("/ward")
    public ResponseEntity<List<Ward>> getAllWard() {
        try {
            List<Ward> wards = new ArrayList<Ward>();
            wardRepository.findAll().forEach(wards::add);
            return new ResponseEntity<>(wards, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    // gọi ward theo id

    @GetMapping("/ward/{id}")
    public ResponseEntity<Ward> getAllWardById(@PathVariable("id") int id) {
        Optional<Ward> ward = wardRepository.findById(id);
        if (ward.isPresent()) {
            return new ResponseEntity<>(ward.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    // tạo provice
    @PostMapping("/province")
    public ResponseEntity<Object> createProvince(@RequestBody Province provinces) {

        try {

            return new ResponseEntity<>(provinceService.getCreateProvince(provinces), HttpStatus.CREATED);
        } catch (Exception e) {

            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create specified province: " +
                            e.getCause().getCause().getMessage());
        }
    }

    // tạo district theo id lưu vào province
    @PostMapping("/province/{id}/district")
    public ResponseEntity<Object> createDistrict(@PathVariable("id") int id, @RequestBody District district) {
        try {
            return new ResponseEntity<>(districtServirce.getCreateDistrict(id, district), HttpStatus.CREATED);
        } catch (Exception e) {
            // TODO: handle exception
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create specified province: " +
                            e.getCause().getCause().getMessage());
        }
    }

    // tạo ward theo id lưu vào district
    @PostMapping("/province/district/{id}/ward")
    public ResponseEntity<Object> createWard(@PathVariable("id") int id, @RequestBody Ward ward) {
        Optional<District> district = districtRepository.findById(id);
        Optional<Ward> ward1 = wardRepository.findById(ward.getId());
        if (district.isPresent()) {
            try {

                if (ward1.isPresent()) {
                    return ResponseEntity.unprocessableEntity().body(" ward already exsit  ");
                }
                District district1 = district.get();
                ward.setDistrict(district1);
                Ward ward2 = wardRepository.save(ward);

                return new ResponseEntity<>(ward2, HttpStatus.CREATED);
            } catch (Exception e) {

                return ResponseEntity.unprocessableEntity()
                        .body("Failed to Create specified ward: " + e.getCause().getCause().getMessage());
            }

        } else {
            return ResponseEntity.badRequest().body("Failed to get specified id: " + id);
        }

    }

    // sửa province theo id
    @PutMapping("/province/{id}") // Dùng phương thức PUT
    public ResponseEntity<Object> updateProvinceById(@PathVariable("id") int id,
            @RequestBody Province province) {
        if (provinceService.GetupdateProvinceById(id, province) != null) {
            return provinceService.GetupdateProvinceById(id, province);
        } else {
            return ResponseEntity.badRequest().body("Failed to get specified Provice: " + id + "  for update.");
        }
    }

    // sửa district theo id
    @PutMapping("/district/{id}") // Dùng phương thức PUT
    public ResponseEntity<Object> updateDistrictById(@PathVariable("id") int id,
            @RequestBody District district) {
        try {
            return new ResponseEntity<>(districtServirce.getUpdateDistrictById(id, district), HttpStatus.CREATED);
        } catch (Exception e) {
            // TODO: handle exception
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create specified province: " +
                            e.getCause().getCause().getMessage());
        }
    }

    // sửa ward theo id
    @PutMapping("/ward/{id}") // Dùng phương thức PUT
    public ResponseEntity<Object> updateWardById(@PathVariable("id") int id,
            @RequestBody Ward ward) {
        Optional<Ward> ward1 = wardRepository.findById(id);
        if (ward1.isPresent()) {
            Ward ward2 = ward1.get();
            ward2.setName(ward.getName());
            ward2.setPrefix(ward.getPrefix());

            Ward ward3 = wardRepository.save(ward2);
            try {
                return new ResponseEntity<>(ward3, HttpStatus.OK);
            } catch (Exception e) {
                return ResponseEntity.unprocessableEntity()
                        .body("Failed to Update specified Provice:" + e.getCause().getCause().getMessage());
            }
        } else {
            return ResponseEntity.badRequest().body("Failed to get specified Provice: " + id + "  for update.");
        }
    }

    // xóa province theo id
    @DeleteMapping("/province/{id}") // Dùng phương thức DELETE
    public ResponseEntity<Province> deleteProvinceById(@PathVariable("id") int id) {
        try {
            provinceRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // xóa district theo id
    @DeleteMapping("/district/{id}") // Dùng phương thức DELETE
    public ResponseEntity<Province> deleteDistrictById(@PathVariable("id") int id) {
        try {
            districtRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // xóa ward theo id
    @DeleteMapping("/ward/{id}") // Dùng phương thức DELETE
    public ResponseEntity<Province> deleteWardById(@PathVariable("id") int id) {
        try {
            wardRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
