package com.devcamp.rest_api.Model;

import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "district_table")
public class District {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column(name = "district_name")
    private String name;

    @Column(name = "district_prefix")
    private String prefix;

    @OneToMany(mappedBy = "district", cascade = CascadeType.ALL)
    @JsonManagedReference
    private Set<Ward> ward;

    @ManyToOne
    @JoinColumn(name = "province_id")
    // @JsonIgnore
    @JsonBackReference
    private Province province;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public Set<Ward> getWard() {
        return ward;
    }

    public void setWard(Set<Ward> ward) {
        this.ward = ward;
    }

    public Province getProvince() {
        return province;
    }

    public void setProvince(Province province) {
        this.province = province;
    }

    public District() {
    }

    public District(int id, String name, String prefix, Set<Ward> ward, Province province) {
        this.id = id;
        this.name = name;
        this.prefix = prefix;
        this.ward = ward;
        this.province = province;
    }

}
