package com.devcamp.rest_api.Repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.rest_api.Model.Province;

public interface ProvinceRepository extends JpaRepository<Province, Integer> {

    Optional<Province> findById(Province province);

}
