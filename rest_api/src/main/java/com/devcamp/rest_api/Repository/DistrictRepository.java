package com.devcamp.rest_api.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.rest_api.Model.District;

public interface DistrictRepository extends JpaRepository<District, Integer> {

}
